import re
from functools import reduce
from urllib.parse import parse_qs, urlparse

import dateparser
from scrapy.loader import ItemLoader
from scrapy.loader.processors import TakeFirst
from scrapy.utils.misc import extract_regex


def deep_get(dictionary, *keys):
    return reduce(lambda d, key: d.get(key) if d else None, keys, dictionary)


def filter_query_param(param):
    def _filter_query_param(url):
        query = parse_qs(urlparse(url).query)
        return query.get(param)

    return _filter_query_param


def as_price(value):
    return int(value.replace('\xa0', '').replace('.', '').replace('€', '').strip())


def filter_includes(subtext):
    return lambda text: text if subtext in text else None


def filter_match(regexp):
    return lambda text: extract_regex(regexp, text)


def strip(subtext):
    return lambda text: str.strip(text, subtext)


def as_date(languages):
    def _as_date(text):
        return str(dateparser.parse(text, languages=languages))

    return _as_date


def as_local_id(spider_name):
    def _as_local_id(text):
        return '%s:%s' % (spider_name, text)

    return _as_local_id


def as_type():
    has_appartement = r"[aA]ppartement"
    has_maison = r"[mM]aison"

    def _as_type(text):
        if re.search(has_appartement, text):
            return 'appartement'
        elif re.search(has_maison, text):
            return 'maison'
        else:
            return 'unknown'

    return _as_type


class ProductLoader(ItemLoader):
    default_output_processor = TakeFirst()