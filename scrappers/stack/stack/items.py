# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy.loader.processors import Join, MapCompose, TakeFirst


def filter_price(value):
    if value.isdigit():
        return value


class ProductItem(scrapy.Item):
    type = scrapy.Field()
    name = scrapy.Field()
    price = scrapy.Field()
    sold = scrapy.Field()
    rooms = scrapy.Field()
    bedrooms = scrapy.Field()
    description = scrapy.Field()
    surface = scrapy.Field()
    city = scrapy.Field()
    zipCode = scrapy.Field()
    published_at = scrapy.Field()

    _raw = scrapy.Field()
    local_id = scrapy.Field()
    url = scrapy.Field()
    created_at = scrapy.Field()
    replaced_at = scrapy.Field()
    hash = scrapy.Field()
    archived = scrapy.Field()
    # TODO: biens actuellement en vente, (même surface, même localization)
    # TODO: ajouter photos
    # TODO: ajouter avec ou sans traveaux
    # TODO: enlever programme neuf

class StackItem(scrapy.Item):
    # define the fields for your item here like:
    localId = scrapy.Field(
        output_processor=TakeFirst(),
    )
    created_at = scrapy.Field(
        output_processor=TakeFirst(),
    )
    replaced_at = scrapy.Field(
        output_processor=TakeFirst(),
    )

    name = scrapy.Field(
        output_processor=TakeFirst(),
    )
    price = scrapy.Field(
        # input_processor=MapCompose(filter_price),
        output_processor=TakeFirst(),
    )
    criteria = scrapy.Field(
        output_processor=TakeFirst(),
    )
    description = scrapy.Field(
        output_processor=TakeFirst(),
    )
    surface = scrapy.Field(
        output_processor=TakeFirst(),
    )
    city = scrapy.Field(
        output_processor=TakeFirst(),
    )
    zipCode = scrapy.Field(
        output_processor=TakeFirst(),
    )
    charges = scrapy.Field(
        output_processor=TakeFirst(),
    )
    _raw = scrapy.Field(
        output_processor=TakeFirst(),
    )

    # sold
    # price
    # estimated_rent
    # updated_at
    # rooms
    # is_new
    # surface
    # type: "appartement", "maison", "immeuble"
    # city:
    # zip_code
