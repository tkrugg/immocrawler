# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html
import json
import hashlib
from pprint import pprint

import pymongo
from datetime import datetime
from bson.objectid import ObjectId
from stack.items import ProductItem
from scrapy.exceptions import DropItem


class HashPipeline(object):
    @classmethod
    def equal_items(cls, item1, item2):
        diff = {}
        for k, v in item2.items():
            if k in ['created_at']:
                continue
            diff[k] = item1[k] == item2[k]
        return all([is_equal for is_equal in diff])

    def process_item(self, item, spider):
        excluded_fields = ['created_at', '_raw', 'hash']
        all_fields = ProductItem.fields.keys()
        for field in excluded_fields:
            if field not in all_fields:
                raise Exception("Excluded field '%s' invalid" % field)
        dict_item = {k: v for k, v in dict(item).items() if k not in excluded_fields}

        json_str = json.dumps(dict_item, sort_keys=True)
        md5_hash = hashlib.md5(json_str.encode('utf-8')).hexdigest()
        item["hash"] = md5_hash
        return item


class EscapePipeline(object):
    @classmethod
    def escape_keys(cls, input):
        def _escape_key(k):
            return k.replace('.', '&#46;').replace('$', '&#36;')

        output = {}
        for k, v in input.items():
            _v = v
            if type(v) is dict:
                _v = cls.escape_keys(v);
            output[_escape_key(k)] = _v
        return output

    @classmethod
    def equal_items(cls, item1, item2):
        diff = {}
        for k, v in item2.items():
            if k in ['created_at']:
                continue
            diff[k] = item1[k] == item2[k]
        return all([is_equal for is_equal in diff])

    def process_item(self, item, spider):
        escaped_item = self.escape_keys(dict(item))
        return escaped_item


class MongoDBPipeline(object):
    crawls_collection_name = "crawls"

    def __init__(self, mongo_uri, mongo_db, collection_name):
        self.mongo_uri = mongo_uri
        self.mongo_db = mongo_db
        self.collection_name = collection_name

    @classmethod
    def from_crawler(cls, crawler):
        # TODO: add crawl_id
        return cls(
            mongo_uri=crawler.settings.get('MONGO_URI'),
            mongo_db=crawler.settings.get('MONGO_DATABASE'),
            collection_name=crawler.settings.get('MONGO_COLLECTION')
        )

    @classmethod
    def equal_items(cls, item1, item2):
        if item1.get('hash') and item2.get('hash'):
            return item1.get('hash') == item2.get('hash')

        diff = {}
        for k, v in item2.items():
            if k in ['created_at']:
                continue
            diff[k] = item1.get(k) == item2.get(k)
        return all([is_equal for is_equal in diff])

    def retrieve_known_urls(self, collection):
        # TODO: exclude archived: true
        return collection.aggregate([
            {
                '$group': {
                    '_id': '$local_id',
                    'local_id': {
                        '$first': '$local_id'
                    },
                    'url': {
                        '$first': '$url'
                    },
                }
            }
        ])

    def open_spider(self, spider):
        self.client = pymongo.MongoClient(self.mongo_uri)
        self.db = self.client[self.mongo_db]
        self.crawl = {'spider': spider.name, 'startedAt': datetime.now(), "endedAt": None}
        self.crawl_id = self.db['crawls'].insert_one(self.crawl).inserted_id
        self.collection_name = 'items_%s' % spider.name
        spider.known_urls = self.retrieve_known_urls(self.db[self.collection_name])

    def close_spider(self, spider):
        self.db['crawls'].update_one(
            {'_id': self.crawl_id},
            {'$set': {'endedAt': datetime.now()}}
        )
        self.client.close()


class MongoDBDuplicatesPipeline(MongoDBPipeline):
    def process_item(self, item, spider):
        collection = self.db[self.collection_name]
        # escaped_item['created_at'] = datetime.now()
        # check if object with local_id already exists
        exists = collection.find_one({
            'local_id': item['local_id'],
            'replaced_at': None
        })
        if exists:
            # check if they are the same, do nothing
            if MongoDBPipeline.equal_items(exists, item):
                crawl_hash = {'crawl_id': self.crawl_id,
                              'local_id': item['local_id'],
                              'hash': item['hash'],
                              'created_at': datetime.now()}
                self.db['crawls_hash'].insert_one(crawl_hash)
                raise DropItem("Duplicate item found: %s:%s" % (item['local_id'], item['hash']))
            else:
                # if they are not the same, mark existing replaced_at date and insert new one
                collection.update_one({
                    'local_id': item['local_id'],
                    'replaced_at': None
                }, {
                    '$set': {'replaced_at': datetime.now()}
                })

        item["created_at"] = datetime.now()
        item['replaced_at'] = None
        collection.insert_one(item)
        crawl_hash = {
            'crawl_id': self.crawl_id,
            'hash': item['hash'],
            'created_at': datetime.now(),
            'local_id': item['local_id']
        }
        self.db['crawls_hash'].insert_one(crawl_hash)
        return item
