from operator import itemgetter

from scrapy import Spider
from stack.items import ProductItem
from scrapy.loader.processors import MapCompose, Join
from stack.utils import as_price, as_date, as_local_id, as_type, ProductLoader, filter_query_param

class PapSpider(Spider):
    name = "pap"
    allowed_domains = ["pap.fr"]
    known_urls = []
    start_urls = [
        "https://www.pap.fr/annonce/vente-appartements-aubervilliers-93300-g43321"
    ]

    def parse(self, response):
        self.logger.info('Crawling known_urls')
        for known_product in self.known_urls:
            local_id, url = itemgetter('local_id', 'url')(known_product)
            yield response.follow(url, self.parse_product_known, meta={
                'handle_httpstatus_list': [404],
                'local_id': local_id,
                'url': url,
            })

        self.logger.info('Crawling for new urls')
        for product in response.css('.main-content .search-list-item a.item-title'):
            yield response.follow(product, self.parse_product)

        for next_page in response.css('.pagination .next a'):
            yield response.follow(next_page, self.parse)

    def parse_product_known(self, response):
        if response.status in [404]:
            item = ProductItem()
            item['local_id'] = response.meta['local_id']
            item['url'] = response.meta['url']
            item['archived'] = True
            return item
        else:
            return self.parse_product(response)

    def parse_product(self, response):
        product_item = ProductLoader(item=ProductItem(), response=response)

        product_item.add_xpath('local_id', "//a[contains(., 'Imprimer')]/@href",
                               MapCompose(filter_query_param('id'), as_local_id(self.name)))

        if not product_item.load_item().get('local_id'):
            return None

        product_item.add_css('url', 'link[rel=canonical]::attr(href)')
        product_item.add_css('type', "h1.item-title::text", MapCompose(str.strip, as_type()))
        product_item.add_css('name', "h1.item-title::text", MapCompose(str.strip))
        product_item.add_css('city', ".item-description h2::text", MapCompose(str.strip), re=r"(.*)\s+\(\d+\)")
        product_item.add_css('zipCode', ".item-description h2::text", MapCompose(str.strip), re=r"\((\d+)\)")
        product_item.add_css('description', ".item-description p::text", MapCompose(), Join())
        product_item.add_css('published_at', ".item-date::text", MapCompose(str.strip, as_date(['fr'])),
                             re=r"\s+\/\s+(.+)$")
        product_item.add_css('rooms', "ul.item-tags li ::text", MapCompose(str.strip, int), re=r"(\d+)\s+pièces?")
        product_item.add_css('bedrooms', "ul.item-tags li ::text", MapCompose(str.strip, int), re=r"(\d+)\s+chambres?")
        product_item.add_css('surface', "ul.item-tags li ::text", MapCompose(str.strip, int), re=r"(\d+)\s+m²")
        product_item.add_css('price', '.item-price::text', MapCompose(as_price))

        return product_item.load_item()
