from functools import reduce

from scrapy import Spider
from scrapy.loader import ItemLoader
from datetime import datetime
from stack.items import StackItem
import json
from urllib.parse import urlparse, parse_qs

def deep_get(dictionary, *keys):
    return reduce(lambda d, key: d.get(key) if d else None, keys, dictionary)

class LaforetSpider(Spider):
    name = "laforet"
    # allowed_domains = ["ladresse.com"]
    start_urls = [
        "http://www.laforet.com/acheter/achat-appartement"

    ]


    def parse(self, response):
        for product in response.css('#panel1 ul.results-compact li[data-json]'):
            json_string_data = product.css('::attr(data-json)').get()
            try:
                json_data = json.loads(json_string_data)
                yield self.parse_product(json_data, response)
            except json.decoder.JSONDecodeError as e:
                continue;

        for next_page in response.css('.pagination a[aria-label=Next]'):
            yield response.follow(next_page, self.parse)

    def parse_product(self, json_data, response):
        stack_item = ItemLoader(item=StackItem(), response=response)

        stack_item.add_value("source", self.name)
        stack_item.add_value("localId", deep_get(json_data, 'propertyId'))
        stack_item.add_value("name", deep_get(json_data, 'title'))
        stack_item.add_value("description", deep_get(json_data, 'description'))
        stack_item.add_value("surface", deep_get(json_data, 'surface'))
        stack_item.add_value("city", deep_get(json_data, 'city'))
        stack_item.add_value("zipCode", deep_get(json_data, 'postalCode'))
        stack_item.add_value("price", deep_get(json_data, 'price'))

        stack_item.add_value("_raw", json_data)

        stack_item.add_value("created_at", datetime.now())
        stack_item.add_value("replaced_at", None)

        return stack_item.load_item()

