from scrapy import Spider
from scrapy.loader import ItemLoader
from datetime import datetime
from stack.items import StackItem
from urllib.parse import urlparse, parse_qs


class LadresseSpider(Spider):
    name = "ladresse"
    # allowed_domains = ["ladresse.com"]
    start_urls = [
        #"https://www.ladresse.com/catalog/advanced_search_result.php?action=update_search&C_28=Vente&C_28_search=EGAL&C_28_type=UNIQUE&C_65_search=CONTIENT&C_65_type=TEXT&C_65=93300%20AUBERVILLIERS&C_65_temp=93300%20AUBERVILLIERS&C_27_search=EGAL&C_27_type=TEXT&C_27=&C_30_search=COMPRIS&C_30_type=NUMBER&C_30_MIN=&C_30_MAX=&search_id=1642227737937571&&search_id=1642227737937571&page=1"
        "https://www.ladresse.com/type_bien/3-33/a-vendre.html",
    ]


    def parse(self, response):

        for product in response.css('.products-cell'):
            localId = product.css('::attr(data-product-id)').get()
            printUrl = 'https://www.ladresse.com/catalog/products_print.php?products_id=%s' % localId
            yield response.follow(printUrl, self.parse_product)

        for next_page in response.css('a.page_suivante'):
            yield response.follow(next_page, self.parse)

    def parse_product(self, response):
        stack_item = ItemLoader(item=StackItem(), response=response)

        stack_item.add_value("source", self.name)

        stack_item.add_css("name", '.titre_bien h1::text')
        stack_item.add_css("description", '.description_text ::text')

        url = response.css('link[rel=canonical]::attr(href)').get()
        query = parse_qs(urlparse(url).query)
        stack_item.add_value("localId", ''.join(query['products_id']))

        criteria = {}
        for item in response.css('.contain_criteres li'):
            [name, value] = item.css('.col-xs-6 ::text').getall()
            criteria[name] = value
        stack_item.add_value("_raw", criteria)

        stack_item.add_value("surface", criteria.get('Surface', None))
        stack_item.add_value("city", criteria.get('Ville', None))
        stack_item.add_value("zipCode", criteria.get('Code postal', None))
        stack_item.add_value("price", criteria.get('Prix', None))
        stack_item.add_value("charges", criteria.get('Charges forfaitaires', None))

        stack_item.add_value("created_at", datetime.now())
        stack_item.add_value("replaced_at", None)

        return stack_item.load_item()

