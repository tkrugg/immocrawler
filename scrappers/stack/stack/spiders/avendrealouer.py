from operator import itemgetter

from scrapy import Spider
from stack.items import ProductItem
from scrapy.loader.processors import MapCompose, Join
from w3lib.html import remove_tags

from stack.utils import as_price, filter_includes, filter_match, strip, as_date, as_local_id, as_type, ProductLoader

class AvendreoualouerSpider(Spider):
    name = "avendrealouer"
    allowed_domains = ["avendrealouer.fr"]
    known_urls = []
    start_urls = [
        "https://www.avendrealouer.fr/vente/aubervilliers-93/b-appartement/loc-101-42809.html"
    ]

    def parse(self, response):
        self.logger.info('Crawling known_urls')
        for known_product in self.known_urls:
            local_id, url = itemgetter('local_id', 'url')(known_product)
            yield response.follow(url, self.parse_product_known, meta={
                'handle_httpstatus_list': [404],
                'local_id': local_id,
                'url': url,
            })

        self.logger.info('Crawling for new urls')
        for product in response.css('#result-list li[data-adid] a.linkToFd'):
            yield response.follow(product, self.parse_product)

        for next_page in response.css('.pagination a.next'):
            yield response.follow(next_page, self.parse)

    def parse_product_known(self, response):
        if response.status in [404]:
            item = ProductItem()
            item['local_id'] = response.meta['local_id']
            item['url'] = response.meta['url']
            item['archived'] = True
            return item
        else:
            return self.parse_product(response)

    def parse_product(self, response):
        product_item = ProductLoader(item=ProductItem(), response=response)

        product_item.add_css('local_id', "[data-favorite]::attr(data-id)", MapCompose(as_local_id(self.name)))

        if not product_item.load_item().get('local_id'):
            return None

        product_item.add_css('url', 'link[rel=canonical]::attr(href)')
        product_item.add_css('type', ".fd-title h1 ::text", MapCompose(str.strip, as_type()))
        product_item.add_css('name', ".fd-title h1 ::text", MapCompose(str.strip))
        product_item.add_css('city', ".fd-title h1 ::text", MapCompose(str.strip), re=r",(.*)\s+\(\d+\)")
        product_item.add_css('zipCode', ".fd-title h1 ::text", MapCompose(str.strip), re=r"\((\d+)\)")
        product_item.add_css('description', ".property-description-main::text", MapCompose(remove_tags), Join())
        product_item.add_css('published_at', ".property-description-main::text", MapCompose(str.strip, as_date(['fr'])), re="Mise à jour le ([\/0-9]+)")
        product_item.add_css('rooms', ".property-description-characteristics td", MapCompose(remove_tags, str.strip, strip('\r\n'), filter_includes('Pièce'), filter_match(r'(\d+)'), int))
        product_item.add_css('surface', ".property-description-characteristics td", MapCompose(remove_tags, str.strip, strip('\r\n'), filter_includes('Surface:'), filter_match(r'(\d+)'), int))
        product_item.add_css('bedrooms', ".property-description-characteristics td", MapCompose(remove_tags, str.strip, strip('\r\n'), filter_includes('Chambre'), filter_match(r'(\d+)'), int))
        product_item.add_css('price', '#fd-price-val ::text', MapCompose(as_price))

        return product_item.load_item()
